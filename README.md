[![pipeline status](https://git.coop/aptivate/ansible-roles/mysql-server/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/mysql-server/commits/master)

# mysql-server

A role to install, configure and secure [MariaDB] on CentOS 7.

The role is called `mysql-server` for historical reasons.

[MariaDB]: https://en.wikipedia.org/wiki/MariaDB

# Requirements

None.

# Role Variables

## Mandatory

* `mariadb_root_password`: The MySQL root password to configure.
  * Take care of the [password validation plugin] for your chosen passwords.

[password validation plugin]: https://mariadb.com/kb/en/library/password-validation/

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: mysql-server
       mariadb_root_password: "wzgds/:Kf2,g"
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```bash
$ pipenv install --dev
$ pipenv run molecule test
```

# License

* https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

* https://aptivate.org/
* https://git.coop/aptivate
