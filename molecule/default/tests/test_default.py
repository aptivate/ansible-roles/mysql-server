def test_mariadb_packages_installed(host):
    assert all((
        host.package(package).is_installed
        for package in (
            'mariadb',
            'mariadb-server',
            'mariadb-devel',
            'mariadb-libs'
        )
    ))


def test_mariadb_root_config_in_place(host):
    assert host.file('/root/.my.cnf').exists


def test_mariadb_service_status(host):
    assert host.service('mariadb').is_running
    assert host.service('mariadb').is_enabled
